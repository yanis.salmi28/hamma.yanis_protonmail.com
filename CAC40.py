#This code was realized within the framework of the master SIEE, it aims to propose a method of quantitative calculation in finance. I chose this theme because I'm doing my internship at the Banque Postale as an IT data auditor.
#So this is the first time I use gitlab. I usually code on edupython where pre-made libraries are available. I tried to integrate all the files that were useful for this project but some files were blocking (I'm the student with the PC in QWERTY keyboard which works as an french Keyboard), moreover it seems that one of my firewalls is blocking some access.Note that I was present only 1 session, because I had the covid during the first session. I apologize for this inconvenience.

import pandas as pd
import numpy as np
#summary of our data for latex
data = {'Label': ['CAC 40 INDEX'],
        'Period':['1990-2020', '1987-2020', '1975-2020', '1960-2020'],
        'Source': ['YAHOO FINANCE', 'YAHOO FINANCE', 'INSEE', 'OCDE'],
        'Frequence': ['Daily', 'Daily', 'Quarterly', 'Quarterly']}


summary = pd.DataFrame(data, columns = ['Label', 'Period', 'Source', 'Frequence'])
print(summary.to_latex(index=False))  

#FRANCE
#Get data for France  

#I tried to import directly via yahoo finance but it is better if we download with a csv
'''import yfinance as yf
from yahoofinancials import YahooFinancials

#take data from yahoo finance
CAC40_df = yf.download('CAC 40', 
                      start='1990-03-01', 
                      end='2020-12-31', 
                      progress=False)


CAC40_df.head(5)

#info
CAC40_df.to_csv(r'C:/Users/Yanis/Documents/hamma.yanis_protonmail.com/CAC40data.csv', index = False)
'''

CAC40_df = pd.read_csv('C:/Users/Yanis/Documents/hamma.yanis_protonmail.com/CAC40.csv')
CAC40_df = CAC40_df.dropna()
CAC40_df.head()
CAC40_df.index = CAC40_df['Date']
#Keep only close price
CAC40_df = pd.DataFrame(CAC40_df['Close'])
#Rename the column
CAC40_df.columns = ['CAC']

# An index is suppose to be >0, so filter & it allows at the same time to remove "NA"
CAC40_df = CAC40_df.loc[CAC40_df['CAC']>0]

if ploton:
    CAC40_df.plot()
#resample data to quarterly data for better comparisons
CAC40_df.index = pd.to_datetime(CAC40_df.index)
CAC40_df = CAC40_df.resample('Q').sum()
CAC40_df.groupby(['Date']).mean()


#compute daily return
#create a new column return price CAC (rcac)
CAC40_df['rCAC'] = CAC40_df['CAC'] / CAC40_df['CAC'].shift(1) - 1
# remove NaN
CAC40_df = CAC40_df.dropna()

ploton = True
import matplotlib.pyplot as plt
#CAC 40 index:
if ploton:
    ax = CAC40_df['CAC'].plot( title='CAC 40 index')
    fig = ax.get_figure()
    fig.savefig('cac40.jpg')
    #plt.close()
  
#CAC 40 returns
if ploton: 
    ax = CAC40_df['rCAC'].plot( title='CAC 40 returns')
    fig = ax.get_figure()
    fig.savefig('rcac40.jpg')
    #plt.close()

#%% are the CAC returns normally distributed?

# visual inspection
# create a new series from a normal distribution that has the mean and the standard deviation of the CAC daily returns
# generate datas issued from a normal law of means & standard deviation
CAC40_df['rCACnormal'] = np.random.normal(CAC40_df.rCAC.mean(), CAC40_df.rCAC.std(), len(CAC40_df))

# Do the two time series have the same mean?
# Two-sample t-test
import scipy.stats
scipy.stats.stats.ttest_ind(CAC40_df.rCAC, CAC40_df.rCACnormal)
# H0: the 2 independent samples have identical average (expected) values
# pvalue is 0.368 < 0.5, we do reject H0

#We want to plot both histogram onee above the other, are the return following a normal law from what you can observe?

if ploton==1:
    plt.hist(CAC40_df.rCAC, 100, alpha=0.5, density=True, label='r')
    plt.hist(CAC40_df.rCACnormal, 100, alpha=0.5 , density=True, label='r if normal')
    plt.legend(loc='upper right')
    plt.title('CAC 40 return versus normal law PDF')
    plt.savefig('normal_cac_hist.jpg')
    plt.show()

    from scipy.stats import kurtosis
from scipy.stats import skew
from scipy.stats import jarque_bera
JBstat = (len(CAC40_df) / 6) * skew(CAC40_df['rCAC'])**2 + (len(CAC40_df) / 24 ) * (kurtosis(CAC40_df['rCAC']) - 3)**2
JBstat
#ok... 18061 = not normal at all
JBstat2 = scipy.stats.jarque_bera(CAC40_df['rCAC'])
JBstat2
# compare it to a chi 2 at 95% with two degrees of freedom
scipy.stats.chi2.ppf(q = 0.95, df = 2)
# JBstat > chi 2 so it's not normal and we reject H0.
del JBstat

#%%Kolmogorov - Smirnov test are the distribution the "same"? 
from scipy import stats
from scipy.stats import kstest 
scipy.stats.ks_2samp(CAC40_df.rCAC, CAC40_df.rCACnormal)

# directly, compare your empirical distribution with the normal distribution
scipy.stats.kstest(CAC40_df.rCAC, 'norm')

# show the cumulative distribution function
if ploton:
    fig, ax = plt.subplots(ncols=1)
    ax1 = ax.twinx()
    ax.hist(CAC40_df.rCAC, 100, alpha=0.2,cumulative = True,color = 'blue')
    ax1.hist(CAC40_df.rCAC, 100, alpha=0.2,normed=True, color = 'blue')
    ax.hist(CAC40_df.rCACnormal, 100, alpha=0.2,cumulative = True,color = 'red')
    ax1.hist(CAC40_df.rCACnormal, 100, alpha=0.2,normed=True,color = 'red')
    plt.title('PDF and CDF, normal law in red')
    fig.savefig('cdf_hist_cacnrom.jpg')
#en rouge c'est la loi normale. Le komogonolv ne rejette pas la normalité, c'est près. 
    #bleu = empirique
if ploton:
    fig, ax = plt.subplots(ncols=1)
    ax.hist(CAC40_df.rCAC, 100, alpha=0.2,cumulative = True,color = 'blue')
    ax.hist(CAC40_df.rCACnormal, 100, alpha=0.2,cumulative = True,color = 'red')
    plt.title('CDF, normal law in red')
    fig.savefig('cdf_cacnrom.jpg')


#%%####################################### UNIT ROOT TEST#############
#The fastest is to use directly ADF without the variants (constant, non-constant, etc.)
statsmodels.tsa.stattools.adfuller(CAC40_df['rCAC'].dropna())

#Other ways to try unit root:GERMANY Withe Noise
wn = pd.DataFrame(CAC40_df.rCAC.mean() + np.random.normal(0, CAC40_df.rCAC.std(), len(CAC40_df)))
wn.columns = ['Withe Noise']
if ploton:
    ax = wn['Withe Noise'].iloc[:300,].plot(title = 'White Noise with the mean and SD of the CAC 40 daily returns')
    fig = ax.get_figure()
    fig.savefig('whitenoiseCAC40.png')

# unit root test
from statsmodels.tsa.stattools import adfuller
adfuller(wn['Withe Noise'],regression='c')

# manual test to investigate the significance of the trend component
# we put Delta Y, Y t-1 and Delta Y t-1
dfadf = pd.concat([wn.diff(1),wn.shift(1),wn.diff(1).shift(1)],axis = 1)
dfadf.columns = ['DY','Y1','DY1']
dfadf = dfadf.dropna(how="any", axis=0)
dfadf.reset_index(inplace=True, drop=True)
dfadf['trend'] = dfadf.index + 1

# we perform the OLS:
import statsmodels.formula.api as smf
resultadf = smf.ols('DY ~  trend + Y1 + DY1',data = dfadf).fit()
resultadf.summary()

del dfadf, resultadf

#%% Trend stationary model because otherwise we can't use ARIMA.
#because he computed initial AR coefficients are not stationary
#that is why we are inducing stationarity here.

wn['trend stationary'] = wn['Withe Noise'] + 0.001 * wn.index


if ploton:
    ax = wn['trend stationary'].iloc[:300,].plot(title = 'Trend stationary model with the mean and SD of the CAC 40 daily returns')
    fig = ax.get_figure()
    fig.savefig('trendstationaryCAC40.jpg')

