import pandas as pd
import numpy as np
#summary of our data for latex
data = {'Label': ['CAC 40 INDEX', 'DAX INDEX', 'France GDP', 'Germany GDP'],
        'Period':['1990-2020', '1987-2020', '1975-2020', '1960-2020'],
        'Source': ['YAHOO FINANCE', 'YAHOO FINANCE', 'INSEE', 'OCDE'],
        'Frequence': ['Daily', 'Daily', 'Quarterly', 'Quarterly']}


summary = pd.DataFrame(data, columns = ['Label', 'Period', 'Source', 'Frequence'])
print(summary.to_latex(index=False))  


CAC40_df.index = CAC40_df['Date']
#Keep only close price
CAC40_df = pd.DataFrame(CAC40_df['Close'])
#Rename the column
CAC40_df.columns = ['CAC']

# An index is suppose to be >0, so filter & it allows at the same time to remove "NA"
CAC40_df = CAC40_df.loc[CAC40_df['CAC']>0]

if ploton:
    CAC40_df.plot()

